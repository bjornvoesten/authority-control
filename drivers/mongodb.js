/**
 * @link https://node-postgres.com
 */

require('../support.js');

const MongoClient = require("mongodb").MongoClient;

class MongoDb {

    constructor() {
        this.name = 'mongodb';

        const uri = 'mongodb://localhost:27017/authority-control';

        this.client = new MongoClient(uri, {
            useNewUrlParser: true
        });
    }

    async connect() {
        await this.client.connect();

        this.database = this.client.db('authority');
    }

    async disconnect() {
        await this.client.close();
    }

    async firstTerm() {
        return await this.database.collection('terms').findOne();
    }

    async collectionExists(name) {
        const collections = await this.database.listCollections().toArray()
        return Promise.resolve(!!collections.find((collection) => collection.name === name));
    }

    async migrate() {
        if (await this.collectionExists('terms')) {
            await this.database.collection('terms').drop();
            await this.database.createCollection('terms');
            await this.database.collection('terms').createIndex({value: "text"})
        }

        if (await this.collectionExists('types')) {
            await this.database.collection('types').drop();
            await this.database.createCollection('types');
        }

        if (await this.collectionExists('alternatives')) {
            await this.database.collection('alternatives').drop();
            await this.database.createCollection('alternatives');
            await this.database.collection('alternatives').createIndex({value: "text"})
        }
    }

    async seed() {
        const types = require('../dataset/authority-types.json');

        await this.database
            .collection('types')
            .insertMany(
                Object
                    .values(types)
                    .map(({id, ...value}) => ({
                        '_id': id, ...value,
                    }))
            );

        const terms = require('../dataset/authority-terms.json');

        await this.database
            .collection('terms')
            .insertMany(
                Object
                    .values(terms)
                    .map(({id, ...value}) => ({
                        '_id': id, ...value,
                    })));

        const alternatives = require('../dataset/authority-alternatives.json');

        await this.database
            .collection('alternatives')
            .insertMany(
                Object
                    .values(alternatives)
                    .map(({id, ...value}) => ({
                        '_id': id, ...value,
                    })));
    };

    async canAssociateArtistName() {
        const term = await this.firstTerm();

        const artist = (await this.database
            .collection('artists')
            .insertOne({'name_term_id': term._id}))
            .ops[0];

        return artist.name_term_id === term._id;
    }

    async canRetrieveArtistName() {
        let artist = this.database
            .collection('artists')
            .findOne();

        const term = this.database
            .collection('terms')
            .findOne({"_id": artist.name_term_id});

        artist = {
            ...artist,
            name: term.value,
        };

        return artist.name === term.value;
    }

    async canSearchArtistTerm() {
        const term = await this.firstTerm();

        const artist = await this.database
            .collection('artists')
            .findOne({
                name_term_id: {
                    $in: (await this.database
                        .collection('terms')
                        .find({
                            $or: [{
                                _id: this.database
                                    .collection('terms')
                                    .find({$text: {$search: term.value}})
                                    .map((item) => item._id)
                                    .toArray()
                            }, {
                                _id: {
                                    $in: await this.database
                                        .collection('alternatives')
                                        .find({$text: {$search: term.value}})
                                        .map((item) => item.term_id)
                                        .toArray()
                                }
                            }]
                        })
                        .map((item) => item._id)
                        .toArray()),
                },
            });

        return term.name_term_id === term.id;
    }

    async canSearchTerm() {
        const query = 'Demanet';

        const terms = await this.database
            .collection('terms')
            .find({
                $or: [{
                    _id: this.database
                        .collection('terms')
                        .find({$text: {$search: query}})
                        .map((item) => item.id)
                        .toArray()
                }, {
                    _id: {
                        $in: await this.database
                            .collection('alternatives')
                            .find({$text: {$search: query}})
                            .map((item) => item.term_id)
                            .toArray()
                    }
                }]
            })
            .sort({'_id': -1})
            .toArray();

        return terms.length === 2
            && terms[0].value === 'Demanet, Armand'
            && terms[1].value === 'Demanet, Victor Joseph Ghislain';
    }

}

module.exports = MongoDb;
