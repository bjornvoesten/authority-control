/**
 * @link https://node-postgres.com
 */

require('../support.js');

class MsSql {

    constructor() {
        this.name = 'mssql';

        this.driver = require('mssql');
    }

    async connect() {
        await this.driver.connect({
            user: 'sa',
            password: 'Password1_',
            server: 'localhost',
            options: {
                enableArithAbort: false,
            },
            pool: {
                max: 10,
                min: 0,
                connectionTimeoutMillis: 60 * 60 * 10,
            },
        });
    }

    async disconnect() {
        await this.driver.close();
    }

    async findTerm(id) {
        const {recordset: rows} = await this.driver.query('select top 1 * from terms where id = ?', [id]);
        return rows.length ? rows[0] : null;
    }

    async firstTerm() {
        const {recordset: rows} = await this.driver.query('select top 1 * from terms');
        return rows.length ? rows[0] : null;
    }

    async migrate() {
        await this.driver.query(`drop database if exists authority`);
        await this.driver.query(`create database authority`);

        await this.driver.query(`drop table if exists types`);
        await this.driver.query(`create table types (id varchar(255), name varchar(255), title varchar(255))`);

        await this.driver.query(`drop table if exists terms`);
        await this.driver.query(`create table terms (id varchar(255), type_id varchar(255), value varchar(255))`);

        await this.driver.query(`drop table if exists alternatives`);
        await this.driver.query(`create table alternatives (id varchar(255), term_id varchar(255), value varchar(255))`);

        await this.driver.query(`drop table if exists artists`);
        await this.driver.query(`create table artists (id int identity(1,1) primary key, name_term_id varchar(255))`);
    }

    async seed() {
        const types = require('../dataset/authority-types.json');

        for (const item of types) {
            await this.driver.query(`insert into types values ('${item.id}', '${item.name}', '${item.title}')`);
        }

        const terms = require('../dataset/authority-terms.json');

        for (const chunk of terms.chunk(500)) {
            const values = chunk
                .map((term) => `('${term.id}', '${term.type_id}', '${term.value.replaceAll('\'', '\'\'')}')`)
                .join(', ');
            await this.driver.query(
                `insert into terms values ${values}`
            );
        }

        const alternatives = require('../dataset/authority-alternatives.json');

        for (const chunk of alternatives.chunk(500)) {
            const values = chunk
                .map((alternative) => `('${alternative.id}', '${alternative.term_id}', '${alternative.value.replaceAll('\'', '\'\'')}')`)
                .join(', ');

            await this.driver.query(`insert into alternatives values ${values}`);
        }
    };

    async canAssociateArtistName() {
        const term = await this.firstTerm();

        // Todo: Connect Postgres database.

        await this.driver.query(`insert into artists (name_term_id) values ('${term.id}')`);

        const {recordset: rows} = await this.driver.query(`select top 1 * from artists order by id desc`);

        if (!rows.length) {
            return false;
        }

        return rows[0].name_term_id === term.id;
    }

    async canRetrieveArtistName() {
        const term = await this.firstTerm();

        // Todo: Connect Postgres database.

        const {recordset: rows} = await this.driver.query(`
            select top 1 artists.id, terms.value as name from artists 
            left join terms on artists.name_term_id = terms.id
        `);

        if (!rows.length) {
            return false;
        }

        return rows[0].name === term.value;
    }

    async canSearchArtistTerm() {
        const term = await this.firstTerm();

        // Todo: Connect Postgres database.

        const {recordset: rows} = await this.driver.query(`       
            select * from artists where exists ( 
            select terms.id from terms where (
                exists (
                    select alternatives.term_id, alternatives.value 
                    from alternatives 
                    where alternatives.term_id = terms.id
                    and alternatives.value like '%${term.value}%'
                ) or terms.value like '%${term.value}%'
            ) and terms.id = artists.name_term_id 
            )
        `);

        if (!rows.length) {
            return false;
        }

        return rows[0].name_term_id === term.id;
    }

    async canSearchTerm() {
        const query = 'Demanet';

        const {recordset: terms} = await this.driver.query(`
        select terms.id, terms.value from terms where type_id in (
            select id from types where name = 'artist'
        ) and (
            exists (
                select alternatives.term_id, alternatives.value from alternatives 
                    where alternatives.term_id = terms.id and alternatives.value like '%${query}%'
            ) or terms.value like '%${query}%'
        )
    `);

        return terms.length === 2
            && terms[0].value === 'Demanet, Armand'
            && terms[1].value === 'Demanet, Victor Joseph Ghislain';
    }

}

module.exports = MsSql;
