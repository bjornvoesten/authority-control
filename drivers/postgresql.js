/**
 * @link https://node-postgres.com
 */

require('../support.js');

const {Client} = require('pg');

class PostgreSql {

    constructor() {
        this.driver = new Client({
            user: 'user',
            host: '127.0.0.1',
            database: 'authority-control',
            password: 'password',
            port: 5432,
        })

        this.name = 'postgresql';
    }

    async connect() {
        this.driver.connect();
    }

    async disconnect() {
        await this.driver.end();
    }

    async findTerm(id) {
        const {rows} = await this.driver.query('select * from terms where id = ? limit 1', [id]);
        return rows.length ? rows[0] : null;
    }

    async firstTerm() {
        const {rows} = await this.driver.query('select * from terms limit 1');
        return rows.length ? rows[0] : null;
    }

    async migrate() {
        await this.driver.query(`drop database if exists authority`);
        await this.driver.query(`create database authority`);

        await this.driver.query(`drop table if exists types`);
        await this.driver.query(`create table types (id varchar(255), name varchar(255), title varchar(255))`);

        await this.driver.query(`drop table if exists terms`);
        await this.driver.query(`create table terms (id varchar(255), type_id varchar(255), value varchar(255))`);

        await this.driver.query(`drop table if exists alternatives`);
        await this.driver.query(`create table alternatives (id varchar(255), term_id varchar(255), value varchar(255))`);

        await this.driver.query(`drop table if exists artists`);
        await this.driver.query(`create table artists (id serial primary key, name_term_id varchar(255))`);
    }

    async seed() {
        const types = require('../dataset/authority-types.json');

        for (const item of types) {
            await this.driver.query(`insert into types values ('${item.id}', '${item.name}', '${item.title}')`);
        }

        const terms = require('../dataset/authority-terms.json');

        for (const chunk of terms.chunk(500)) {
            let variables = 0;

            const query = chunk
                .map(() => {
                    const query = `($${variables + 1}, $${variables + 2}, $${variables + 3})`;
                    variables = variables + 3;
                    return query;
                })
                .join(', ');

            const values = chunk.reduce((list, item) => [...list, item.id, item.type_id, item.value], [])

            await this.driver.query(`insert into terms values ${query}`, values);
        }

        const alternatives = require('../dataset/authority-alternatives.json');

        for (const chunk of alternatives.chunk(500)) {
            let variables = 0;

            const query = chunk
                .map(() => {
                    const query = `($${variables + 1}, $${variables + 2}, $${variables + 3})`;
                    variables = variables + 3;
                    return query;
                })
                .join(', ');

            const values = chunk.reduce((list, item) => [...list, item.id, item.term_id, item.value], [])

            await this.driver.query(`insert into alternatives values ${query}`, values);
        }
    };

    async canAssociateArtistName() {
        const term = await this.firstTerm();

        await this.driver.query(`insert into artists (name_term_id) values ('${term.id}')`);

        const {rows} = await this.driver.query(`select * from artists order by id desc limit 1`);

        if (!rows.length) {
            return false;
        }

        return rows[0].name_term_id === term.id;
    }

    async canRetrieveArtistName() {
        const term = await this.firstTerm();

        const {rows} = await this.driver.query(`
            select artists.id, terms.value as name from artists 
            left join terms on artists.name_term_id = terms.id limit 1
        `);

        if (!rows.length) {
            return false;
        }

        return rows[0].name === term.value;
    }

    async canSearchArtistTerm() {
        const term = await this.firstTerm();

        const {rows} = await this.driver.query(`       
            select * from artists where exists ( 
                select terms.id from terms where (
                    exists (
                        select alternatives.term_id, alternatives.value 
                        from alternatives 
                        where alternatives.term_id = terms.id
                        and alternatives.value like '%${term.value}%'
                    ) or terms.value like '%${term.value}%'
                ) and terms.id = artists.name_term_id 
            )
        `);

        if (!rows.length) {
            return false;
        }

        return rows[0].name_term_id === term.id;
    }

    async canSearchTerm() {
        const query = 'Demanet';

        const {rows: terms} = await this.driver.query(`
        select terms.id, terms.value from terms where type_id in (
            select id from types where name = 'artist'
        ) and (
            exists (
                select alternatives.term_id, alternatives.value from alternatives 
                    where alternatives.term_id = terms.id and alternatives.value like '%${query}%'
            ) or terms.value like '%${query}%'
        )
    `);

        return terms.length === 2
            && terms[0].value === 'Demanet, Armand'
            && terms[1].value === 'Demanet, Victor Joseph Ghislain';
    }

}

module.exports = PostgreSql;
