const PostgreSql = require('./drivers/postgresql.js');
const MsSql = require('./drivers/mssql.js');
const MongoDb = require('./drivers/mongodb.js');

const run = async () => {

    const databases = [
        new PostgreSql(),
        new MsSql(),
        new MongoDb(),
    ];

    for (const database of databases) {
        await database.connect();

        try {
            console.log('testing', database.name);

            await database.migrate();
            await database.seed();

            let result;

            result = await database.canAssociateArtistName();
            console.log(database.name, 'can associate artist name', result);

            result = await database.canRetrieveArtistName();
            console.log(database.name, 'can retrieve artist name', result);

            result = await database.canSearchArtistTerm();
            console.log(database.name, 'can search artist term', result);

            result = await database.canSearchTerm();
            console.log(database.name, 'can search term', result);

            await database.disconnect();
        } catch (exception) {
            console.error(exception);

            await database.disconnect();
        }
    }

}

run().catch((error) => console.error(error));

