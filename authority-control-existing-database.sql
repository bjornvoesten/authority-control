CREATE TABLE `terms` (
  `id` int NOT NULL AUTO_INCREMENT,
  `value` varchar(255) NOT NULL,
  UNIQUE KEY `terms_primary` (`id`) USING BTREE,
  UNIQUE KEY `terms_value` (`value`) USING BTREE
);

CREATE TABLE `artists` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name_term_id` int NOT NULL,
  UNIQUE KEY `artists_primary` (`id`) USING BTREE,
  KEY `artists_name_term_id` (`name_term_id`) USING BTREE,
  CONSTRAINT `artists_ibfk_1` FOREIGN KEY (`name_term_id`) REFERENCES `terms` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
);

-- Search artists using name term
-- Search artists using name term alternative
-- Search terms using value
-- Search terms using value alternative
-- Display artist name using term value