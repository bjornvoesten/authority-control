# Authority Control Onderzoek

## Set up databases

### PostgreSQL

```
# Create
docker run --name authority-postgresql -e POSTGRES_PASSWORD=password -e POSTGRES_USER=user -e POSTGRES_DB=authority -d -p 5432:5432 postgres

# Start
docker start authority-postgresql

# Stop
docker stop authority-postgresql

# Delete
docker rm authority-postgresql
```

`127.0.0.1` : `5432`
`user` : `password`

### MsSQL

```
# Create
docker run --name=authority-mssql -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=Password1_' -p 1433:1433 -d mcr.microsoft.com/mssql/server:2017-latest-ubuntu 

# Start
docker start authority-mssql

# Stop
docker stop authority-mssql

# Delete
docker rm authority-mssql
```

`127.0.0.1` : `1433`
`sa` : `Password1_`

### MongoDB

```
# Create
docker run --name authority-mongodb -e MONGO_INITDB_DATABASE=authority -d -p 27017:27017 mongo:4.4.5

# Start
docker start authority-mongodb

# Stop
docker stop authority-mongodb

# Delete
docker rm authority-mongodb
```

`mongodb://localhost:27017/authority-control`

### Redis

```
# Create
docker run --name authority-redis -d -p 6379:6379 redislabs/redisearch:latest

# Start
docker start authority-redis

# Stop
docker stop authority-redis

# Delete
docker rm authority-redis
```

`127.0.0.1` : `6379`

## Testing

`npm run test`
